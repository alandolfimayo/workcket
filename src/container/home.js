import React, { useState, useEffect, useRef } from 'react';
import ListProducts from '../components/List';
import Modal from '../components/Modal';
import * as Api from '../api';
import {
  Container,
  Header,
  HeaderTitle,
  HeaderSubtitle,
  ButtonAgregar,
} from './styled';

const Home = () => {

  const [count, setCount] = useState(0);
  const [products, setproducts] = useState([]);
  const [valid, setValid] = useState(false);
  const inputEl = useRef(null);
  const [modal, setModal] = useState(false);

  useEffect(() => {
    Api.getItems().then((data) => {
      setproducts(data);
      setCount(data.length);
    });
  }, []);

  useEffect(() => {
    setValid(false);
  }, [modal]);

  useEffect(() => {
    setCount(count + products.length);
  }, [products]);

  const onValidate = (e) => {
    setValid(e.target.value !== '');
  };

  const handleAddProduct = () => {
     return valid && Api.addItem(inputEl.current.value,count).then((item) => {
      setproducts([...products, item]);
      setModal(false)
    });
  };

  const handleDelete = (value) => {
    setproducts(products.filter(i => i.id !== parseInt(value.id)));
    return Api.removeItem(value).then((item) => {
      setproducts(products.filter(i => i.id !== parseInt(item.id)));
    });
  };

  const handleModal = () => {
    return setModal(!modal);
  };

  return (
    <Container>
      <Header>
        <HeaderTitle> Supermarket List </HeaderTitle>
        <HeaderSubtitle> {products.length} items </HeaderSubtitle>
      </Header>

      <ListProducts items={products} onDelete={handleDelete} />
     
      <ButtonAgregar onClick={handleModal}>
        Agregar Item
      </ButtonAgregar>

      {modal && (
        <Modal
          inputEl={inputEl}
          onChangeValidation={onValidate}
          onClickCancel={handleModal}
          onClickAddItem={handleAddProduct}
          valid={valid}
        />
      )}
    </Container>
  );
};


export default Home;