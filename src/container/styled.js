import styled from 'styled-components';

export const Container = styled.main`
  background: #f5f5f5;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction:column;
`;

export const Header = styled.header`
  margin-bottom: 40px;
  margin-top:60px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

export const HeaderTitle = styled.label`
  color: #c2c2c2;
  font-weight: bold;
  font-size: 32px;
  flex: 1 1 100%;
  text-align: center;
`;

export const HeaderSubtitle = styled.h2`
  color: black;
  text-transform: uppercase;
  margin-top: 15px;
`;


export const ButtonAgregar = styled.button`
  background: #1da5fc;
  margin-bottom:20px;
  width:600px;
  color: white;
  border-radius: 5px;
  flex: 1 1 100%;
  min-height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 18px;
  cursor: pointer;
  transition: 0.3s ease;
  margin-top: 40px;
  &:hover{
    background: #1783c8;
  }
`;
