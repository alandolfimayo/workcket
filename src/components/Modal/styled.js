import styled from 'styled-components';

export const ContainerModal = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 40px;
  width: 100%;
  background: white;
  border-radius: 5px;
  box-shadow: 0px 2px 5px rgba(0,0,0,0.1);
  margin: 20px 0;
  max-width: 400px;
`;


export const ContainerButtons = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  flex: 1 1 100%;
  margin-top: 20px;
`;

export const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(243, 243, 243, 0.82);
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.div`
  flex: 1 1 100%;
  margin-bottom: 20px;
  font-size: 20px;
  text-align: center;
`;
export const AddItemInput = styled.input`
  display: flex;
  border: 1px solid #cbcbcb;
  border-radius: 5px;
  min-height: 50px;
  flex: 1 1 100%;
  padding: 10px;
  box-sizing: border-box;
`;
export const ButtonCancel = styled.div`
  display: flex;
  border: 1px solid #cbcbcb;
  min-height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 18px;
  cursor: pointer;
  color: #404040;
  flex: 1;
  max-width: 125px;
  border-radius: 5px;
`;
export const ButtonConfirm = styled.div`
  display: flex;
  transition: 0.3s ease;
  background: ${props => (props.active ? '#1da5fc' : '#cbcbcb')};
  cursor: ${props => (props.active ? 'pointer' : 'not-allowed')};
  border: none;
  min-height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 18px;
  color: white;
  flex: 1;
  max-width: 125px;
  border-radius: 5px;
`;