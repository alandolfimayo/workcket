import React from 'react';
import PropTypes from 'prop-types';

import {
  Container,
  AddItemInput,
  ButtonCancel,
  Title,
  ButtonConfirm,
  ContainerButtons,
  ContainerModal,
} from './styled';

const Modal = ({
  inputEl,
  onChangeValidation,
  onClickCancel,
  onClickAddItem,
  valid,
}) => {
  return (
    <Container>
      <ContainerModal>
        <Title> Nuevo Articulo </Title>
        <AddItemInput type="text" ref={inputEl} onChange={onChangeValidation} />
        <ContainerButtons>
          <ButtonCancel onClick={onClickCancel}>
            Cancelar
          </ButtonCancel>
          <ButtonConfirm onClick={onClickAddItem} active={valid}>
            Aceptar
          </ButtonConfirm>
        </ContainerButtons>
      </ContainerModal>
    </Container> 
  );
};


Modal.propTypes = {
  inputEl: PropTypes.object,
  onChangeValidation: PropTypes.func,
  onClickCancel: PropTypes.func,
  onClickAddItem: PropTypes.func,
  valid: PropTypes.bool,
};

Modal.defaultProps = {
  inputEl: null,
  handleAddValidation: null,
  handleModal: null,
  handleAddItem: null,
  valid: false,
};

export default Modal;
