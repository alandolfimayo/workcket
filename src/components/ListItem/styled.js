
import styled from 'styled-components';

export const Item = styled.li`
display: flex;
flex-basis: 100%;
justify-content: space-between;
padding: 20px;
background: white;
border-radius: 5px;
box-shadow: 0px 2px 5px rgba(0,0,0,0.1);
font-size: 18px;
align-items: center;
margin-top: 15px;
&:hover{
  box-shadow: 0px 5px 5px rgba(0,0,0,0.1);
};`;

export const ButtonDelete = styled.button`
border: none;
width: 30px;
height: 30px;
padding: 0;
cursor: pointer;
background:white;
`;