import React, { lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-loader-spinner'
import {
  ListContainer,
  ListItemsContainer
} from './styled';

const ListItem =  lazy(() => import('../ListItem'));

const List = props => {
  const { items, onDelete } = props
  return (
    <Suspense fallback={<Spinner type="Puff"
    color="#00BFFF"
    height="100"	
    width="100" />}>
      <ListContainer>
        <ListItemsContainer>
          {
            (items.length > 0) ? items.map(product => (
              <ListItem
                key={product.id}
                name={product.label}
                handleDelete={() => onDelete(product)}
              />
            ))
            : (<label>No hay articulos para mostrar</label>)
          }
        </ListItemsContainer>
      </ListContainer>
    </Suspense> 
  );
};

List.propTypes = {
  items: PropTypes.array
};

List.defaultProps = {
  items: [],
};

export default List;