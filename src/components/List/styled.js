import styled from 'styled-components';

export const ListContainer = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  max-width: 600px;
  flex-wrap: wrap;
`;

export const ListItemsContainer = styled.ul`
  display: flex;
  flex-direction: column;
  flex-basis: 100%;
`;

