import { fill } from 'lodash';

export const createItems = () => {
  const arr = fill(new Array(3), {id:0,label:''});
  return arr.map((value,key) => ({
    id:key,
    label: 'label '+ key
  }))
}