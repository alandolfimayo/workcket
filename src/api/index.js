import * as _ from './utils';

const getList = () => {
  if( localStorage.hasOwnProperty('items')){
    console.log("entre getlist")
    return JSON.parse(localStorage.getItem('items'));
  }
  return _.createItems();
};

export const getItems = () => {
  return new Promise((resolve) => setTimeout(() => {
   resolve(getList()) 
  },1000));
};

export const addItem = (label,count) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      const item = {
        id: count + 1,
        label,
      };
      const List = getList().concat(item);
      localStorage.setItem('items', JSON.stringify(List));
      resolve(item);
    },1000);
  });
};

export const removeItem = (value) => {
  const items = getList(); 
  const item = items.find(i => i.id === value.id);
  return new Promise((resolve) => {
    setTimeout(() => {
      const newList = items.filter(i => i.id !== value.id);
      localStorage.setItem('items', JSON.stringify(newList));
      resolve(item);
    },1000);
  });
};
